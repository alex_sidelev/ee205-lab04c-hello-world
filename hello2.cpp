//////////////////////////////////////////////////////////////////////////////
/////// University of Hawaii, College of Engineering
/////// EE 205  - Object Oriented Programming
/////// Lab 04c - Hello World
///////
/////// @file hello2.cpp
/////// @version 1.0
///////
/////// Prints Hello World using C++ objects
///////
/////// @author Alexander Sidelev <asidelev@hawaii.edu>
/////// @brief  Lab 04c - Hello-World-2 - EE 205 - Spr 2021
/////// @date   02_09_2021
///////////////////////////////////////////////////////////////////////////////////

#include <iostream>

   int main() {
      std::cout << "Hello World!"<< std::endl;
   return 0;
   }





















//////////////////////////////////////////////////////////////////////////////
///// University of Hawaii, College of Engineering
///// EE 205  - Object Oriented Programming
///// Lab 04c - Hello World
/////
///// @file Хелло.cpp
///// @version 1.0
/////
///// Печатает Здравствуйте! при помоши Ц++
/////
///// @author Alexander Sidelev <asidelev@hawaii.edu>
///// @brief  Lab 04c - Здравствуй Мир! - EE 205 - Spr 2021
///// @date   02_09_2021
/////////////////////////////////////////////////////////////////////////////////

// declare object standard library 
#include <iostream>
using namespace std;
  
   int main(){ 
    cout<<"Здравствуй Мир!"<< endl;     
   return 0; 
   } 
















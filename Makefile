###############################################################################
###############################################################################
# # Uversity of Hawaii, College of Engineering
# # EE 205  - Object Oriented Programming
# # Lab 03b - Animal Farm 1
# #
# # @file Makefile
# # @version 1.0
# #
# # @author Alexander Sidelev <asidelev@hawaii.edu>
# # @brief  Lab 04c - Hello World - EE 205 - Spr 2021
# # @date   02_09_2021
# ###############################################################################
CC     = g++

TARGET1 = hello1
TARGET2 = hello2

all: $(TARGET1) $(TARGET2)

hello1: hello1.cpp
	$(CC) -o $(TARGET1) hello1.cpp

hello2: hello2.cpp
	$(CC) -o $(TARGET2) hello2.cpp

clean:
	   rm -f *.o $(TARGET1) $(TARGET2)


